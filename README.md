git clone https://gitlab.com/deepak.bhatnagar/assignment.git

Install dependencies
:- npm install
 
:- node index.js 

Server is listening on port 8081

All right! Let’s now run the server and go to http://localhost:8081 to access the route we just defined.